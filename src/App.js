import './App.css';
import StateLists from './container/StateLists'
import StateInformation from './container/StateInformation';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/' exact element={<StateLists />} />
          <Route path='/states/:statesId' exact element={<StateInformation />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;