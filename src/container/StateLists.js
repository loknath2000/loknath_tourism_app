import React from 'react'
import axios from 'axios'
import { useEffect } from 'react'
import { useDispatch} from 'react-redux'
import StateComponent from './StateComponent'
import { setStates } from '../redux/actions/stateAction'

const StateLists = () => {
  const dispatch = useDispatch()

  
  useEffect(() => {
    const fetchStates = async () => {
      const response = await axios.get('https://tourism-india-app.herokuapp.com/states')
      .catch((err) =>{
        console.log(err)
      } )
      dispatch(setStates(response.data))
    }
    fetchStates()
  }, [])

  return (
    <>
    <div className='quote'><img src='https://i.pinimg.com/originals/17/3b/55/173b55514aaa8598b828c63909a158a0.jpg' alt='img'></img></div>
    <div className='states'>
      <StateComponent />
    </div>
    </>
  )
}

export default StateLists