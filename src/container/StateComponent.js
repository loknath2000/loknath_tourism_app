import React,{useState} from 'react'
import '../App.css'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'

const StateComponent = () => {
  const states = useSelector((state) => state.allStates.states)
  const [search,setSearch] = useState("")
  const changeHandler = (e) =>{
    setSearch(e.target.value)
  }
  // states.filter(city=>city.name.toLowerCase().includes(search.toLowerCase()))
  const renderStates = states.filter(city=>city.name.toLowerCase().includes(search.toLowerCase())).map((state) => {
    const { state_id, name, image_url } = state
    return (
      <>
        <div className='state grow' key={state_id}>
          <div className='cards'>
        <Link to={`/states/${state_id}`}>
            <div className='image'>
              <img src={image_url} alt={name} />
              <div className='content'>
                <button>Know More</button>
              </div>
            </div>
            <h3><i className="fa-solid fa-location-crosshairs"></i>{name}</h3>
          </Link>
          </div>
        </div>
      </>
    )
  })
  return (
    <>
    <div className='input'>
        <h2>Enter the state</h2>
        <input className="search" placeholder='Search for the State' type='text' value={search} onChange={changeHandler}/>
    <br/>
    <div className='listStates'>
      {renderStates}
    </div>
    </div>
    </>
  )
}

export default StateComponent