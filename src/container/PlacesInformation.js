import React from 'react'

const PlaceInformation = (props) => {

    return (
        <div className='container'>
            <div className='placesCard'>
                <div className='placeInfo'>
                    <img src={props.image} alt={props.name} />
                    <div className='placeContent'>
                        <h3><i className="fa-solid fa-map-location"></i>{props.name}</h3>
                        <p>{props.description}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PlaceInformation