import React, { useEffect } from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { selectedStates, removeSelectedStates } from '../redux/actions/stateAction'
import PlaceInformation from './PlacesInformation'
const StateDetails = () => {
  // const state = useSelector(states=> states.state)
  
  const state = useSelector((states) => Object.values(states.state))
  console.log(state)
  const { statesId } = useParams() 
  
  //statesId should be same which me mention in app.js
  // console.log("***",state)

  const dispatch = useDispatch()
  const fetchStatesPlaces = async () => {
    const response = await axios.get(`https://tourism-india-app.herokuapp.com/states/${statesId}/places`)
      .catch((err) => {
        console.log(err)
      })
    dispatch(selectedStates(response.data))
  }
  useEffect(() => {
    if (statesId && statesId !== "") fetchStatesPlaces()
    return () => { dispatch(removeSelectedStates()) }
  }, [statesId])
  return (
    <div className='places'>

      {state.map((object) => {
        return <PlaceInformation name={object.name} description={object.description} image={object.image_url} />
      })}

    </div>
  )
}

export default StateDetails