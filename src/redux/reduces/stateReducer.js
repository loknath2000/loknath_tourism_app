

const initialState={
    states:[],
}

export const statesReducer=(state=initialState,{type,payLoad})=>{
    switch(type){
        case "SET_STATES":
            return {...state,states:payLoad}//assigning empty states array to Payload that is url from setlists.js
        default:
            return state
    }
}

export const selectedStatesReducer=(state={},{type,payLoad})=>{
    switch(type){
        case "SELECTED_STATES":
            return {...state,...payLoad}
        case "REMOVE_SELECTED_PRODUCT":
            return {}
        default:
            return state
    }
}