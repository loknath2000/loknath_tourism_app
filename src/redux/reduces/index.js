import { combineReducers } from "redux";
import { statesReducer,selectedStatesReducer } from "./stateReducer";

const reducers=combineReducers({
    allStates:statesReducer,
    state:selectedStatesReducer,
})

export default reducers